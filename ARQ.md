## architecture

Sometimes we want to define thresolds to the consumption of messages. This scenario can be real when an unexpected or malformed message is produced to a topic, or downstream and upstream services are operating in degraded mode or they're even completetely down. It can cause consumer lag, chaos in the infrastructure, and possibly customer impact depending on the actual SLA.

This library attempts to find an optimal solution for this issue. 

### Topics

In the current implementation, a topic has 3 variants, the main topic, one to handle retried messages and the other to serve as a dead-letter.

#### Main Topic

It is the topic known by the clients interested, commonly used to emit business events, such as company creations, changes, etc.

Example: `company-created` - 3 partitions

#### Retry topic

This topic is a variant of the main topic, utilized whenever a message consumption fails. In other words, when the first consumption of the message fails, the library is going to publish this failed message to the retry topic, decrementing the quantity of retries from its content, it keeps going until it gets succesfully consumed or reaches the number of retries - if that occurs, this message is published to another topic, check below.

Example: `company-created-<service-name>-retries`

#### Dead Letter topic

This is topic is known as a "limbo", every message that can't be processed even after the reattempts, fall into the dead-letter. The dead-letter is monitored in real-time and is consumed by a manual script, to be later analysed.

Example: `company-created-<service-name>-dl`

#### Naming convention

Attaching the `service-name` to the topic variant allowed us to have separate retried consumptions by service. Most of the PubSub services can have N consumers subscribed to the topics and this is not different in Kafka. In this case, the usage of `service-name` allows every service consuming from the topic to have its own retried messages. Just because eventually a message fails in a given service, does NOT mean it is going to fail its consumption in another.

If we don't use this type of identification, we could have services reconsuming messages without the need.

#### Diagram

<img src="./topic.jpg" height="400" width="600">



