'use strict';

const test = require('ava');

const Consumer = require('../lib/consumer');
const Producer = require('../lib/producer');
const Admin = require('../lib/admin');
const WG = require('wait-group-promise');

const topic1 = 'teste-consumer01';
const topic1Retries = 'teste-consumer01-slim-kafka-retries';
const topic1DL = 'teste-consumer01-slim-kafka-dl';


const createMsg = () => {
  return {
    payload: {
      destination: '900',
      numbers: [Math.random() *10000000 + 1]
    }
  };
};

test.before(async (t) => {
  t.context.admin = new Admin({
    'bootstrap.servers':'localhost:9092'
  });
});

test.beforeEach(async (t) => {
  if (t.context.admin) {
    const producer = new Producer({
      rdKafkaBrokerOpts: {
        'bootstrap.servers': 'localhost:9092'
      }
    });
    producer.on('error', (error) => { 
      console.log(error);
      process.exit(1);
    });
    await producer.init();
    
    t.context.producer = producer;

    await Promise.all([
      t.context.admin.deleteTopic(topic1),
      t.context.admin.deleteTopic(topic1Retries), 
      t.context.admin.deleteTopic(topic1DL)
    ]);
  }
});

test.afterEach.always(async (t) => {
  if (t.context.admin) {
    await t.context.producer.dispose();
    await Promise.all([
      t.context.admin.deleteTopic(topic1),
      t.context.admin.deleteTopic(topic1Retries), 
      t.context.admin.deleteTopic(topic1DL)
    ]);
  }
});

test.after.always(async (t) => {
  if (t.context.admin) {
    await t.context.admin.dispose();
  }
});

test('Should consume message from topic', async (t) => {
  const wg = new WG();
  wg.add(1);

  await t.context.admin.client.createTopic({
    topic: topic1,
    num_partitions: 1,
    replication_factor: 1
  });

  const msgExpected = {
    payload: {
      destination: '900',
      numbers: ['904823423']
    }
  };

  const consumer = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  });
  consumer.on('error', wg.done);
  await consumer.init({ 
    topic: topic1,
    concurrency: 1,
    retries: 10,
    serviceName: 'slim-kafka',
    handler: (msg) => {
      t.deepEqual(msg, msgExpected);
      wg.done();
    }
  });

  await t.context.producer.publish(topic1, msgExpected);
  await wg.wait();
  await consumer.dispose();
});

test('Should consume message with distinct consumers for the same topic and group', async (t) => {
  const wg = new WG();
  wg.add(5);

  await Promise.all([
    t.context.admin.client.createTopic({
      topic: topic1,
      num_partitions: 2,
      replication_factor: 1
    }),
    t.context.admin.client.createTopic({
      topic: topic1Retries,
      num_partitions: 2,
      replication_factor: 1
    })
  ]);

  const msg1 = createMsg();
  const msg2 = createMsg();
  const msg3 = createMsg();
  const msg4 = createMsg();
  const msg5 = createMsg();

  const consumer = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  });
  consumer.on('error', wg.done);

  await consumer.init({ 
    topic: topic1,
    retries: 10,
    concurrency: 2,
    serviceName: 'slim-kafka',
    handler: (msg) => wg.done()
  });
    
  await t.context.producer.publish(topic1, msg1);
  await t.context.producer.publish(topic1, msg2);
  await t.context.producer.publish(topic1, msg3);
  await t.context.producer.publish(topic1, msg4);
  await t.context.producer.publish(topic1, msg5);

  await wg.wait();

  t.pass();
  await consumer.dispose();
});

test('Should test consumer concurrency', async (t) => {
  const wg = new WG();
  wg.add(10000);

  await Promise.all([
    t.context.admin.client.createTopic({
      topic: 'teste',
      num_partitions: 10,
      replication_factor: 1
    })
  ]);

  const msg1 = createMsg();

  for (let i = 0; i < 10000; i++) {
    t.context.producer.publish('teste', msg1);
  }
  
  const consumer = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group2'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  });
  consumer.on('error', wg.done);

  await consumer.init({ 
    topic: 'teste', 
    concurrency: 5,
    retries: 5,
    serviceName: 'slim-kafka',
    handler: (msg) => {
      wg.done(); 
      return;
    } 
  });

  await wg.wait();
  t.pass();

  await consumer.dispose();
});

test('Should test the consumption failure and the message retry on the dead-letter', async (t) => {
  const wg = new WG();
  wg.add(1);

  await Promise.all([
    t.context.admin.client.createTopic({
      topic: topic1,
      num_partitions: 5,
      replication_factor: 1
    }),
    t.context.admin.client.createTopic({
      topic: topic1Retries,
      num_partitions: 5,
      replication_factor: 1
    })
  ]);
  
  await t.context.producer.publish(topic1, {
    teste: 'retrying'
  });
  
  const consumer = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group5'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  }, t.context.producer);
  
  consumer.on('error', wg.done);

  let c = 0;
  consumer.on('info', (msg, details) => {
    if (details && details[0] && details[0].topic) {
      if (c > 1) {
        t.is(details[0].topic, topic1Retries);
      }  else {
        t.is(details[0].topic, topic1);
      }
    }
  });

  await consumer.init({ 
    topic: topic1, 
    concurrency: 5, 
    retries: 10,
    serviceName: 'slim-kafka',
    handler: (msg) => {
      c++;
      if (c == 5) {
        wg.done();
      } else {
        throw new Error('Failed to process the message');
      }
    }
  });

  await wg.wait();
  t.pass();

  await consumer.dispose();
});

test('Should test the consumption failure and the message retry exceeded', async (t) => {
  const wg = new WG();
  wg.add(13);

  const [topic, retries, dl] = ['teste2', 'teste2-slim-kafka-retries', 'teste2-slim-kafka-dl'];

  await Promise.all([
    t.context.admin.client.createTopic({
      topic,
      num_partitions: 10,
      replication_factor: 1
    }),
    t.context.admin.client.createTopic({
      topic: retries,
      num_partitions: 10,
      replication_factor: 1
    })
  ]);
  
  t.context.producer.on('delivery-report', (msg, details) => {
    t.true([topic, retries, dl].includes(details.topic));
  });
  await t.context.producer.publish(topic, {
    teste: 'retrying'
  });
  
  const consumer = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group5'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  }, t.context.producer);
  
  consumer.on('error', wg.done);

  consumer.on('info', (msg, details) => {
    if (details && details[0] && details[0].topic) {
      t.true([topic, retries, dl].includes(details[0].topic));
      wg.done();
    }
  });

  await consumer.init({ 
    topic, 
    concurrency: 5, 
    retries: 10,
    serviceName: 'slim-kafka',
    handler: (msg) => {
      throw new Error('Failed to process the message');
    }
  });

  const consumerDL = new Consumer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092',
      'group.id': 'teste-group5'
    },
    rdKafkaTopicOpts: {
      'auto.offset.reset': 'earliest'
    }
  }, t.context.producer);
  
  consumerDL.on('error', wg.done);

  consumerDL.on('info', (msg, details) => {
    if (details && details[0] && details[0].topic) {
      t.is(details[0].topic, dl);
      wg.done();
    }
  });

  await consumerDL.init({ 
    topic: dl, 
    concurrency: 1, 
    retries: 0,
    serviceName: 'slim-kafka',
    handler: (msg) => {
      t.deepEqual(msg, {
        teste: 'retrying'
      });
      return;
    }
  });

  await wg.wait();
  t.pass();

  await Promise.all([ consumer.dispose(), consumerDL.dispose() ]);
});
