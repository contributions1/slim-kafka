#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Pretest
# Intended to be run from local machine or CI
set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v docker >/dev/null 2>&1 || { echo 'please install docker or use image that has it'; exit 1; }
command -v docker-compose >/dev/null 2>&1 || { echo 'please install docker-compose or use image that has it'; exit 1; }

log() {
  echo """
################################################################################
  $1

  """
}

##################
# PRETEST script #
##################

log "Stopping and removing docker-compose"
docker-compose -f test/helpers/docker-compose.yml down

log "Running docker-compose"
docker-compose -f test/helpers/docker-compose.yml up -d

sleep 10

log "Started."