'use strict';

const test = require('ava');

const Producer = require('../lib/producer');
const Admin = require('../lib/admin');
const WG = require('wait-group-promise');

const topic1 = 'teste-consumer01';
const topic2 = 'teste-consumer02';


test.before(async (t) => {
  t.context.admin = new Admin({
    'bootstrap.servers': 'localhost:9092'
  });
});

test.beforeEach(async (t) => {
  if (t.context.admin) {
    await Promise.all([t.context.admin.deleteTopic(topic1), t.context.admin.deleteTopic(topic2)]);
  }
});

test.afterEach.always(async (t) => {
  if (t.context.admin) {
    await Promise.all([t.context.admin.deleteTopic(topic1), t.context.admin.deleteTopic(topic2)]);
  }
});

test.after.always(async (t) => {
  if (t.context.admin) {
    await t.context.admin.dispose();
  }
});

test('Should produce message on a topic', async (t) => {
  const wg = new WG();
  wg.add(1);

  await t.context.admin.client.createTopic({
    topic: topic1,
    num_partitions: 1,
    replication_factor: 1
  });

  const msgExpected = {
    payload: {
      destination: '900',
      numbers: ['904823423']
    }
  };

  const producer = new Producer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092'
    }
  });
  producer.on('error', wg.done);
  await producer.init();

  producer.on('delivery-report', (msg, details) => {
    t.is(msg, 'Received delivery-report event from the KafkaProducer');
    t.deepEqual(details.value, msgExpected);
    t.pass();
    wg.done();
  });
  await producer.publish(topic1, msgExpected);

  await wg.wait();
  await producer.dispose();
});


test('Should produce message on a topic with multiple partitions', async (t) => {
  const wg = new WG();
  wg.add(100);

  await t.context.admin.client.createTopic({
    topic: topic1,
    num_partitions: 20,
    replication_factor: 1
  });

  const msgExpected = {
    payload: {
      destination: '900',
      numbers: ['904823423']
    }
  };

  const producer = new Producer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092'
    }
  });
  producer.on('error', wg.done);
  await producer.init();

  producer.on('delivery-report', (msg, details) => {
    t.is(msg, 'Received delivery-report event from the KafkaProducer');
    t.deepEqual(details.value, msgExpected);
    t.true(details.partition >= 0 && details.partition <= 20);
    wg.done();
  });
  for (let i = 0; i < 100; i++) {
    await producer.publish(topic1, msgExpected);
  }

  await wg.wait();
  await producer.dispose();
});

test('Should retry publishing the message on topic when the rdkafka internal queue is full', async (t) => {
  const wg = new WG();
  wg.add(120001);

  await t.context.admin.client.createTopic({
    topic: topic1,
    num_partitions: 20,
    replication_factor: 1
  });

  const msgExpected = {
    payload: {
      destination: '900',
      numbers: ['904823423']
    }
  };

  const producer = new Producer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092'
    }
  });

  producer.on('warn', (error) => {
    wg.done();
  });
  producer.on('delivery-report', (msg, details) => {
    wg.done();
  });
  producer.on('error', wg.done);
  await producer.init();

  for (let i = 0; i < 120000; i++) {
    await producer.publish(topic1, msgExpected);
  }

  await wg.wait();

  t.pass();
  await producer.dispose();
});

test('Should shutdown the producer gracefully', async (t) => {
  const wg = new WG();
  wg.add(1);

  await t.context.admin.client.createTopic({
    topic: topic1,
    num_partitions: 20,
    replication_factor: 1
  });

  const msgExpected = {
    payload: {
      destination: '900',
      numbers: ['904823423']
    }
  };

  const producer = new Producer({
    rdKafkaBrokerOpts: {
      'bootstrap.servers': 'localhost:9092'
    }
  });
  producer.on('error', wg.done);
  await producer.init();


  setTimeout(() => {
    producer.dispose().then(() => wg.done()).catch(wg.done);
  }, 0);

  for (let i = 0; i < 100000; i++) {
    await producer.publish(topic1, msgExpected);
  }

  await wg.wait();

  t.pass();
});