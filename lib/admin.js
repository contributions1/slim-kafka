'use strict';

const { AdminClient } = require('node-rdkafka');
const VError = require('verror');


/**
 * @class KafkaAdmin
 */
class Admin {
  constructor(kafkaAdminOpts) {
    this.client = AdminClient.create(kafkaAdminOpts);
  }

  /**
   * Create a topic
   * @param {Object} opts refer to the RDKafka docs
   * @returns
   * @memberof KafkaAdmin
   */
  createTopic(opts) {
    return new Promise((resolve, reject) => {
      this.client.createTopic(opts, (error, data) => {
        if (error) {
          return reject(error);
        }

        resolve(data);
      });
    });
  }


  /**
   * Remove a topic
   * @param {string} topic
   * @returns
   * @memberof KafkaAdmin
   */
  deleteTopic(topic) {
    return new Promise((resolve, reject) => {
      this.client.deleteTopic(topic, (error, data) => {
        if (error) {
          if (error.code == 3) {
            return resolve('Topic already exists');
          }

          return reject(new VError({
            cause: error,
            info: { topic }
          }, 'Failed to delete the topic'));
        } else {
          resolve(data);
        }
      });
    });
  }


  /**
   * Create partitions on a topic
   * @param {opts} opts refer to the RDKafka docs
   * @returns
   * @memberof KafkaAdmin
   */
  createPartitions(topic, partitions = 1, timeout = 100) {
    return new Promise((resolve, reject) => {
      this.client.createPartitions(topic, partitions, timeout, (error, data) => {
        if (error) {
          return reject(new VError({
            cause: error,
            info: { topic, partitions }
          }, 'Failed to create partitons for the topic'));
        } else {
          resolve(data);
        }
      });
    });
  }


  /**
   * Disposes the connection with the client
   * @memberof Admin
   */
  dispose() {
    this.client.disconnect();
  }
}

module.exports = Admin;
