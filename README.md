# slim-kafka

Simplified instantiation of Kafka's consumer and producer, built on top of node-rdkafka.

## Architecture

If you want to know more how this library implementation works, check the [`architecture documentation`](./ARQ.md)

## Installation

Install
```
$ npm install --save <private-repo-addr>/slim-kafka.git
```

## Tests

Run the integration tests
```
$ npm test
```


# Classes

 * [`Producer`](#Producer)
 * [`Consumer`](#Consumer)

## Producer

### Methods 

* constructor
    * **kafkaProducerOpts** *(`object`, `required`)* - The producer options
        * **rdKafkaBrokerOpts** *(`object`)* - Broker configuration. Refer to the [`rdkafka docs`](https://github.com/edenhill/librdkafka/blob/0.11.1.x/CONFIGURATION.md).
        * **rdKafkaTopicOpts** *(`object`)* - Topic configuration. Refer to the [`rdkafka docs`](https://github.com/edenhill/librdkafka/blob/0.11.1.x/CONFIGURATION.md).

* *async* init - Initialize the resources.

* publish - Publishes a message on a given topic. 
    * Throws error if the internal RDKafka queue is full. It means that too many messages are stuck there.
    * **topic** *(`string`, `required`)* - *topic* The topic to send the message
    * **message** *(`string`, `required`)* - *message* The message sent to the topic
    * **parition** *(`string`, `optional`, `default=-1`)* - *partition* The partiton of the topic. Sends randomly consistent if not set.


* *async* dispose - Disposes the connection with the broker. 
  * It may be used when implementing a proper app shutdown.
  * Awaits ongoing processes to disconnect from the broker.   

### Events

* on `error` - Emitted when something critical happened with the producer connection.
    * **error** *(`VError`)* - The error itself, instance of VError.

* on `warn` - Emitted when something wrong happened.
    * **error** *(`VError`)* - The error itself, instance of VError.

* on `info` - Emitted the broker exposes statistics for the producer
    * **msg** (`string`) - A simple message describing the info.
    * **args** (`object`) - The object containing the arguments.

* on `delivery-report` - Emitted when a message was published successfully
    * **msg** (`string`) - A simple message explaining the report.
    * **details** (`object`) - The object containing the reports itself.

* on `delivery-report-error` - Emitted when something wrong happened with the message delivery
    * **error** *(`VError`)* - The error itself, instance of VError.

### Example

```javascript
'use strict';

const { Producer } = require('slim-kafka');

async function start() {
  try {
    const producer = new Producer({
      rdKafkaBrokerOpts: {
        'bootstrap.servers': 'localhost:9092'
      }
    });
    producer.on('error', (error) => {
      // handle the error event.
    });
    producer.on('warn', (error) => {
      // handle the warn event.
    });
    producer.on('info', (msg, args) => {
      // obtain producer statistics and extra information
    });
    producer.on('delivery-report', (msg, report) => {
      // obtain the report of message delivery
    });
    producer.on('delivery-report-error', (error) => {
      // obtain the error of message delivery
    });

    await producer.init();

    producer.publish('topico-1', { teste: 'teste' });

    await producer.dispose();
  } catch (error) {
    process.exit(1);
  }
}
```

## Consumer

### Methods 

* constructor
    * **kafkaConsumerOpts** *(`object`, `required`)* - The consumer options
        * **rdKafkaBrokerOpts** *(`object`)* - Broker configuration. Refer to the [`rdkafka docs`](https://github.com/edenhill/librdkafka/blob/0.11.1.x/CONFIGURATION.md).
        * **rdKafkaTopicOpts** *(`object`)* - Topic configuration. Refer to the [`rdkafka docs`](https://github.com/edenhill/librdkafka/blob/0.11.1.x/CONFIGURATION.md).
    * **kafkaProducer** *(`function`, `optional`, `default=null`)* - An instance of KafkaProducer, used to requeue the failed messages.


* *async* init - Inicialização dos recursos.
    * **config** *(`object`)* - The initialization options
        * **serviceName** *(`string`, `required`)* - *serviceName* The name of the microservice.
        * **topic** *(`string`, `required`)* - *topic* The topic to fetch messages from.
        * **handler** *(`func`, `required`)* - *message* The message handler.
        * **concurrency** *(`number`, `required`)* - *concurrency* The quantity of messages consumed at a time
        * **retries** *(`string`, `optional`, `default=3`)* - *retries* The number of attempts to reprocess the failed message

* *async* dispose - Disposes the connection with the broker. 
    * It may be used when implementing a proper app shutdown.
    * Awaits ongoing processes to disconnect from the broker.   

### Events

* on `error` - Emitted when something critical happened with the consumer connection.
    * **error** *(`VError`)* - The error itself, instance of VError.

* on `warn` - Emitted when something wrong happened.
    * **error** *(`VError`)* - The error itself, instance of VError.

* on `info` - Emitted the broker exposes statistics for the consumer
    * **msg** (`string`) - A simple message describing the info.
    * **args** (`object`) - The object containing the arguments.

### Example

```javascript
'use strict';

const { Consumer } = require('slim-kafka');

async function start() {
  try {
    const consumer = new Consumer({
      rdKafkaBrokerOpts: {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'teste-group'
      }
    }, kafkaProducer);

    consumer.on('error', (error) => {
      // handle the error event.
    });
    consumer.on('warn', (error) => {
      // handle the warn event.
    });
    consumer.on('info', (msg, args) => {
      // obtain producer statistics
    });

    await consumer.init({ 
      topic: 'topic-name', 
      concurrency: 10,
      retries: 20,
      serviceName: 'dialer-post',
      handler: async (msg) => {
        // Do something with the message
        // You can throw an error to retry, if that's desired.
      }
    });

    setTimeout(async () => {
      try {
        await consumer.dispose();
      } catch (error) {
        console.error(error);
        process.exit(1);
      }
    }, 60000);
  } catch (error) {
    process.exit(1);
  }
}
```