'use strict';

const Producer = require('./lib/producer');
const Consumer = require('./lib/consumer');
const Admin = require('./lib/admin');

module.exports = {
  Producer,
  Consumer,
  Admin
};
